package se.hakandev.hellowell;

import android.app.Application;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

/**
 * Created by Hakan on 2017-11-29.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        AppCenter.start(MyApplication.this, "583bc581-b8ba-4159-8b7d-ef59bca838d6",
                Analytics.class, Crashes.class);

    }
}
